package com.cr.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import com.cr.classes.Database;

public class ProgressBar extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private ProgressBar frame;
	private JProgressBar progressBar;
	private JLabel downloadingLabel;
	private static int option;
	private static String absolutePath;


	/**
	 * Create the dialog.
	 */
	public ProgressBar(int option, String absolutePath) {

		ProgressBar.option = option;
		ProgressBar.absolutePath = absolutePath;
		frame = this;

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setVisible(true);

		// Center on the screen
		setLocationRelativeTo(null);

		setResizable(false);
		setTitle("Downloading Channels");
		setBounds(100, 100, 450, 146);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		contentPanel.setLayout(null);
		{
			progressBar = new JProgressBar();
			progressBar.setBounds(20, 44, 414, 24);
			progressBar.setValue(0);
			contentPanel.add(progressBar);

			downloadingLabel = new JLabel("<Channels downloading>");
			downloadingLabel.setBounds(20, 11, 414, 24);
			contentPanel.add(downloadingLabel);
		}

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);

				cancelButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.dispatchEvent(new WindowEvent(frame,
								WindowEvent.WINDOW_CLOSING));
					}
				});
			}
		}

		
		new Thread(new Runnable() {
			public void run() {
				// Load ProgressBar
				Database database = new Database();

				try {
					switch (ProgressBar.option) {
						case 1:
							database.readXLSFile(downloadingLabel, progressBar, ProgressBar.absolutePath);
							break;
						case 2:
							database.readWebService(downloadingLabel, progressBar);
							break;
					}
					
					new MainView(database);
					
					frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();

		
	}
}
