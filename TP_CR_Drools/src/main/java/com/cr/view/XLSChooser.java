package com.cr.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class XLSChooser extends JFrame {

	private JTextField filename = new JTextField(), dir = new JTextField();

	private JButton open = new JButton("Open"), save = new JButton("Save");

	public XLSChooser() {

	}

	public void openFile() {

		JFileChooser c = new JFileChooser();
		// Demonstrate "Open" dialog:
		int rVal = c.showOpenDialog(XLSChooser.this);

		if (rVal == JFileChooser.APPROVE_OPTION) {
			filename.setText(c.getSelectedFile().getName());
			dir.setText(c.getCurrentDirectory().toString());
		}
		if (rVal == JFileChooser.CANCEL_OPTION) {
			filename.setText("You pressed cancel");
			dir.setText("");
		}
	}

	public void saveFile() {
		JFileChooser c = new JFileChooser();
		// Demonstrate "Save" dialog:
		int rVal = c.showSaveDialog(XLSChooser.this);
		if (rVal == JFileChooser.APPROVE_OPTION) {
			filename.setText(c.getSelectedFile().getName());
			dir.setText(c.getCurrentDirectory().toString());
		}
		if (rVal == JFileChooser.CANCEL_OPTION) {
			filename.setText("You pressed cancel");
			dir.setText("");
		}
	}
}