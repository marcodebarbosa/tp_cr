package com.cr.view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.cr.classes.Database;

public class Intro extends JFrame {

	private JLabel lblTvadviser;
	private JButton xlsButton, webButton;

	public static void main(String[] args) {

		new Intro();
	}

	public Intro() {

		setTitle("TVAdviser");
		setType(Type.POPUP);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 450);
		setLocationRelativeTo(null);

		getContentPane().setLayout(null);

		lblTvadviser = new JLabel("TVAdviser");
		lblTvadviser.setFont(new Font("Tahoma", Font.PLAIN, 41));
		lblTvadviser.setBounds(127, 27, 185, 43);
		getContentPane().add(lblTvadviser);

		webButton = new JButton("WEB");
		webButton.setBounds(257, 199, 108, 58);
		getContentPane().add(webButton);

		xlsButton = new JButton("XLS");
		xlsButton.setBounds(78, 200, 108, 57);
		getContentPane().add(xlsButton);

		JLabel lblConhecimentoERaciocnio = new JLabel(
				"Conhecimento e Racioc\u00EDnio @ 2014/2015");
		lblConhecimentoERaciocnio.setBounds(116, 374, 249, 23);
		getContentPane().add(lblConhecimentoERaciocnio);

		JLabel lblImportYourDatabase = new JLabel("Import your database from:");
		lblImportYourDatabase.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblImportYourDatabase.setBounds(127, 159, 185, 43);
		getContentPane().add(lblImportYourDatabase);

		registerListeners();

		setVisible(true);

	}

	private void registerListeners() {

		webButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				new ProgressBar(2, null);

			}
		});

		xlsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Handle open button action.
				// Database database = new Database();
				// XLSChooser chooserDialog = new XLSChooser();
				// chooserDialog.saveFile();

				JFileChooser fc = new JFileChooser();

				fc.setAcceptAllFileFilterUsed(false);
				fc.addChoosableFileFilter(new MyFilter());

				int returnVal = fc.showOpenDialog(Intro.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();

					new ProgressBar(1, file.getAbsolutePath());

				}
			}
		});

	}
}

class MyFilter extends javax.swing.filechooser.FileFilter {

	@Override
	public boolean accept(File f) {
		return f.isDirectory() || f.getName().toLowerCase().endsWith(".xls");
	}

	@Override
	public String getDescription() {
		return "*.xls";
	}
}
