package com.cr.classes;

public class Classification implements Comparable<Classification>{

	private int showID;
	private int numberOfKeywordsFound;
	private int classification;

	public Classification(int showID, int classification) {
		super();
		this.showID = showID;
		this.classification = classification;
	}

	public int getShowID() {
		return showID;
	}

	public void setShowID(int showID) {
		this.showID = showID;
	}

	public int getNumberOfKeywordsFound() {
		return numberOfKeywordsFound;
	}

	public void setNumberOfKeywordsFound(int numberOfKeywordsFound) {
		this.numberOfKeywordsFound = numberOfKeywordsFound;
	}

	public int getClassification() {
		return classification;
	}

	public void setClassification(int classification) {
		this.classification = classification;
	}

	@Override
	public int compareTo(Classification arg0) {
		if(classification == arg0.classification)
			return 0;
		
		return 1;
	}
}
