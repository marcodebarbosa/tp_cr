package com.cr.classes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



public class Database {

	Date input_date_start;
	Date input_date_end;
	int input_duration_min;
	String input_genre;
	String input_keyword;
	String input_channel;
	String input_title;
	String input_season;
	String input_episode;

	final public static int WEIGHT_CHANNLE_NAME = 3;
	final public static int WEIGHT_TITLE = 4;
	final public static int WEIGHT_GENRE = 2;
	final public static int WEIGHT_DATE_START = 5;
	final public static int WEIGHT_DATE_END = 5;
	final public static int WEIGHT_KEYWORD = 1;
	final public static int WEIGHT_DURATION = 2;
	final public static int WEIGHT_SEASON = 7;
	final public static int WEIGHT_EPISODE = 7;



	private ArrayList<Channel> arrayChannels;
	private ArrayList<Show> arrayShows;
	private ArrayList<Classification> arrayClassification;

	boolean setup_input[] = new boolean[] {false, false, false, false, false, false, false, false, false}; //check wich values are inputed.


	public Database( Date input_date_start,	
			Date input_date_end, int input_duration_min, 
			String input_genre, String input_keyword, String input_channel, 
			String input_title, String input_season, String input_episode) {
		super();
		this.arrayChannels = new ArrayList<Channel>();
		this.arrayShows = new ArrayList<Show>();
		this.arrayClassification = new ArrayList<Classification>();

		this.input_date_start = input_date_start;
		this.input_date_end = input_date_end;
		this.input_duration_min = input_duration_min;
		this.input_genre = input_genre;
		this.input_keyword = input_keyword;
		this.input_channel = input_channel;
		this.input_title = input_title;
		this.input_season = input_season;
		this.input_episode = input_episode;
	}

	public Database(){
		super();

		this.arrayChannels = new ArrayList<Channel>();
		this.arrayShows = new ArrayList<Show>();
		this.arrayClassification = new ArrayList<Classification>();
		this.input_date_start = new Date();
		this.input_date_end = new Date();
		this.input_duration_min = 0;
		this.input_genre = "";
		this.input_keyword = "";
		this.input_channel = "";
		this.input_title = "";
		this.input_season = "";
		this.input_episode = "";

	}
	public void readWebService(JLabel downloadingLabel, JProgressBar progressBar) throws Exception {

		arrayChannels.clear();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

		GetChannels(arrayChannels);

		boolean isLoaded = false;
		int progressValue = 100 / arrayChannels.size();

		for (int i = 0; i < arrayChannels.size(); i++) {

			System.out.println("");
			System.out.println("Channel: " + i);

			arrayChannels.get(i).setAbreviation(arrayChannels.get(i).getAbreviation().replaceAll("(^ )|( $)", ""));

			downloadingLabel.setText("Downloading " + (i + 1) + "/" + arrayChannels.size() + " ["+ arrayChannels.get(i).getName() + " ]");

			progressBar.setValue(progressBar.getValue() + progressValue);

			String serviceLink = "http://services.sapo.pt/EPG/GetChannelByDateInterval?channelSigla="
					+ arrayChannels.get(i).getAbreviation().trim()
					+ "&startDate=2012-09-11+20%3A00%3A00&endDate=2050-09-12+21%3A00%3A00";

			try {
				System.out.println("Getting channel...");
				URL url = new URL(serviceLink);
				URLConnection conn = url.openConnection();
				DocumentBuilderFactory factory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(conn.getInputStream());
				System.out.println("Channel downloaded.");

				NodeList nList = doc.getElementsByTagName("Program");

				for (int temp = 0; temp < nList.getLength(); temp++) {

					Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						Show show = new Show();
						show.setTitle(getTagValue("Title", eElement));
						show.setDescription(getTagValue("Description", eElement));
						show.setName(getTagValue("ChannelName", eElement));
						show.setGenre(arrayChannels.get(i).getGenre());
						show.setDuration(Integer.parseInt(getTagValue(
								"Duration", eElement)) / 60);
						show.setDateStart(sdf.parse(getTagValue("StartTime",
								eElement)));
						show.setDateEnd(sdf.parse(getTagValue("EndTime",
								eElement)));

						arrayClassification.add(new Classification(
								show.getId(), 0));
						arrayShows.add(show);
					}
				}

				// labelMsg.setText(labelMsg.getText() + "OK!\n");
				isLoaded = true;

			} catch (Exception e) {
				// labelMsg.setText(labelMsg.getText() + "Falhou!\n");
				System.out.println("Channel Failed!");
				System.out.println("Exceptio" + e);
			}

		}

		if (!isLoaded) {
			System.out
			.println("It was possible to stream the information from the service");
			throw new Exception();
		}
	}

	public void readXLSFile(JLabel downloadingLabel, JProgressBar progressBar,
			String inputFile) {

		File inputWorkbook = new File(inputFile);
		Classification classification;

		Workbook w;

		arrayChannels.clear();
		arrayShows.clear();
		arrayClassification.clear();

		try {

			w = Workbook.getWorkbook(inputWorkbook);
			// Get the first sheet
			Sheet sheet = w.getSheet(0);

			Show show;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

			int progressValue = 100 / sheet.getRows();

			for (int i = 1; i < 76; i++) {

				show = new Show();
				show.setName(sheet.getCell(1, i).getContents());
				show.setTitle(sheet.getCell(2, i).getContents());
				show.setDescription(sheet.getCell(3, i).getContents());
				show.setGenre(sheet.getCell(4, i).getContents());

				int foo = Integer.parseInt(sheet.getCell(7, i).getContents());
				show.setDuration(foo);

				// DATA_INICIAL
				Cell valueCell = sheet.getCell(5, i);

				if (valueCell.getType() == CellType.DATE) {
					DateCell dCell = (DateCell) valueCell;

					show.setDateStart(dCell.getDate());

					TimeZone gmtZone = TimeZone.getTimeZone("GMT");
					DateFormat destFormat = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm");
					destFormat.setTimeZone(gmtZone);

					String s = destFormat.format(show.getDateStart());
					System.out.println(s);

				}

				// DATA_FINAL
				valueCell = sheet.getCell(6, i);

				if (valueCell.getType() == CellType.DATE) {

					DateCell dCell = (DateCell) valueCell;

					show.setDateEnd(dCell.getDate());
				}

				classification = new Classification(show.getId(), 0);

				if (show.getId() > -1) {

					if (!existeCanal(show.getName())) {
						Channel novo = new Channel(show.getName(),
								show.getName(), "indefinido");
						arrayChannels.add(novo);
					}

					arrayClassification.add(classification);
					arrayShows.add(show);

				}

				System.out.println("Importing " + (i + 1) + "/"
						+ sheet.getRows() + " [" + show.getName() + " ]");
				downloadingLabel.setText("Importing " + (i + 1) + "/"
						+ sheet.getRows() + " [" + show.getTitle() + " ]");

				progressBar.setValue(progressBar.getValue() + progressValue);

			}

		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	boolean existeCanal(String nomeCanal) {

		for (int i = 0; i < arrayChannels.size(); i++)
			if (arrayChannels.get(i).getName() == nomeCanal)
				return true;

		return false;
	}



	private static String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0)
				.getChildNodes();

		Node nValue = (Node) nlList.item(0);

		return nValue.getNodeValue();
	}

	public boolean checkStartDate(Date start){
		if(input_date_start.compareTo(start)<=0)
			return true;
		return false;
	}

	public boolean checkEndDate(Date end){
		if(input_date_end.compareTo(end)<=0)
			return true;
		return false;
	}



	public void sortClassifications(){
		Classification c;

		for (int i=0; i<arrayClassification.size(); i++)
			for (int j=1; j<arrayClassification.size(); j++)
				if (arrayClassification.get(j).getClassification()>arrayClassification.get(j-1).getClassification()){
					c = arrayClassification.get(j);
					arrayClassification.set(j, arrayClassification.get(j-1));
					arrayClassification.set(j-1, c);
				}
	}


	public boolean checkKeyWord(String description, int id){
		boolean hasFound = false;
		for(int i=0; i<arrayShows.size();i++){
			if(arrayShows.get(i).getId() == id){
				if(!getInput_keyword().equals(""))
					if(description.toLowerCase().contains(getInput_keyword().toLowerCase())){
						System.out.println("Emcncontrei Keyword!");
						arrayShows.get(i).setNr_keywords(arrayShows.get(i).getNr_keywords()+1);
						hasFound = true;
					}
			}
		}

		return hasFound;
	}

	public int getFoundKeyWords(int id){
		for(int i=0; i<arrayShows.size();i++){
			if(arrayShows.get(i).getId() == id){
				return arrayShows.get(i).getNr_keywords();
			}
		}

		return 0;

	}

	public static boolean GetChannels(ArrayList<Channel> arrayChannels)
			throws IOException {
		// Check the channel information on the file, to load from the service
		try {
			BufferedReader in = new BufferedReader(
					new FileReader("canais1.txt"));
			String str1, str2, str3, str;

			while (((str1 = in.readLine()) != null)
					&& ((str2 = in.readLine()) != null)
					&& ((str3 = in.readLine()) != null)) {
				arrayChannels.add(new Channel(str1, str2, str3));

				System.out.println(str1 + str2 + str3);

				if ((str = in.readLine()) == null) {
					break;
				}
			}
			in.close();
		} catch (IOException e) {
			System.out.println(e);
			throw e;
		}
		return true;
	}

	public void resetClassification(){

		for (int i=0; i<arrayClassification.size(); i++){
			arrayClassification.get(i).setClassification(0);
		}

	}

	public void resetParameters(){

		for (int i=0; i<setup_input.length; i++){
			setup_input[i]=false;
		}
	}


	public boolean compareNames(String show_name){

		return show_name.toLowerCase().contains(input_title.toLowerCase());

	}

	public void ClassificationInc(int id, int value)
	{
		for(int i=0; i<arrayClassification.size(); i++)
		{
			if(arrayClassification.get(i).getShowID()==id)
			{
				arrayClassification.get(i).setClassification(arrayClassification.get(i).getClassification()+value);
			}
		}

	}

	public void input_from(int i) {
		if (i>=0 && i<setup_input.length)
			setup_input[i] = true;
	}

	public boolean getSetup_input(int i) {
		return setup_input[i];
	}

	public void setSetup_input(boolean[] setup_input) {
		this.setup_input = setup_input;
	}

	public ArrayList<Channel> getArrayChannels() {
		return arrayChannels;
	}

	public void setArrayChannels(ArrayList<Channel> arrayChannels) {
		this.arrayChannels = arrayChannels;
	}

	public ArrayList<Show> getArrayShows() {
		return arrayShows;
	}

	public void setArrayShows(ArrayList<Show> arrayShows) {
		this.arrayShows = arrayShows;
	}

	public ArrayList<Classification> getArrayClassification() {
		return arrayClassification;
	}

	public void setArrayClassification(ArrayList<Classification> arrayClassification) {
		this.arrayClassification = arrayClassification;
	}

	public Date getInput_date_start() {
		return input_date_start;
	}

	public void setInput_date_start(Date input_date_start) {
		this.input_date_start = input_date_start;
	}

	public Date getInput_date_end() {
		return input_date_end;
	}

	public void setInput_date_end(Date input_date_end) {
		this.input_date_end = input_date_end;
	}

	public int getInput_duration_min() {
		return input_duration_min;
	}

	public void setInput_duration_min(int input_duration_min) {
		this.input_duration_min = input_duration_min;
	}

	public String getInput_genre() {
		return input_genre;
	}

	public void setInput_genre(String input_genre) {
		this.input_genre = input_genre;
	}

	public String getInput_keyword() {
		return input_keyword;
	}

	public void setInput_keyword(String input_keyword) {
		this.input_keyword = input_keyword;
	}

	public String getInput_channel() {
		return input_channel;
	}

	public void setInput_channel(String input_channel) {
		this.input_channel = input_channel;
	}

	public String getInput_title() {
		return input_title;
	}

	public void setInput_title(String input_title) {
		this.input_title = input_title;
	}

	public String getInput_season() {
		return input_season;
	}

	public void setInput_season(String input_season) {
		this.input_season = input_season;
	}

	public String getInput_episode() {
		return input_episode;
	}

	public void setInput_episode(String input_episode) {
		this.input_episode = input_episode;
	}

}
