package com.cr.classes;

import java.util.Date;

public class Show {
	
	public static float X = (float) 0.25;

	private int id;
	private static int idAux = 0;
	private String title;
	private String name;
	private String description;
	private String genre;
	private Date dateStart;
	private Date dateEnd;
	private int duration;
	private int nr_keywords;
	
	public Show(){
		super();
		this.id = idAux;
		idAux++;
	}
			
	public Show(int id, String title, String name, Date dateStart, Date dateEnd, int duration, String description) {
		super();
		this.id = id;
		this.title = title;
		this.name = name;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.duration = duration;
		this.description = description;
		this.nr_keywords = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public boolean DurationOnScale(int duration_inserted)
	{
		if (duration == duration_inserted)
			return true;
		
		return false;
			
	}

	public int getNr_keywords() {
		return nr_keywords;
	}

	public void setNr_keywords(int nr_keywords) {
		this.nr_keywords = nr_keywords;
	}
	
	
	
}
