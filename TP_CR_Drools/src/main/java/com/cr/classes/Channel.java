package com.cr.classes;

public class Channel {

	private int id;
	private static int idAux = 0;
	private String name;
	private String abreviation;
	private String genre;

	public Channel(String name, String abreviation, String genre) {
		super();
		this.id = idAux;
		idAux++;
		this.name = name;
		this.abreviation = abreviation;
		this.genre = genre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbreviation() {
		return abreviation;
	}

	public void setAbreviation(String abreviation) {
		this.abreviation = abreviation;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}
}
