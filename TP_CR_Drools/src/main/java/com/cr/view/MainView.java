package com.cr.view;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.logger.KnowledgeRuntimeLogger;
import org.drools.logger.KnowledgeRuntimeLoggerFactory;
import org.drools.runtime.StatefulKnowledgeSession;

import com.cr.classes.Classification;
import com.cr.classes.Database;
import com.cr.classes.Show;

import javax.swing.JMenuBar;
import javax.swing.SwingConstants;

public class MainView extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField title_text;
	private JTextField channel_text;
	private JTextField genre_text;
	private JTextField duration_text;
	private JTextField start_date_text;
	private JTextField end_date_text;
	private JTextField keyword_text;
	private JTextField episodeField;
	private JTextField seasonField;
	private JScrollPane results;
	private Database database;
	private JTextField keywords;

	//Create the combo box, select item at index 4.
	//Indices start at 0, so 4 specifies the pig.
	private JComboBox genre_combo;


	/**
	 * Create the frame.
	 */
	public MainView(Database db) {

		this.database = db;

		setTitle("TVAdviser");

		setBounds(100, 100, 950, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		table = new JTable();
		//table.setBounds(228, 25, 300, 525);
		table.setModel(new javax.swing.table.DefaultTableModel(
				new Object [][] { },
				new String [] {
						"Recomendation","Channel", "Title", "Genre","Date Start","Date End", "Duration","Description"
				}
				));

		//System.out.println("Canal-> " + database.getArrayChannels().get(0).getName() + " -> "+ database.getArrayChannels().get(0).getGenre() + " - "+ database.getArrayChannels().get(0).getId() + " " + database.getArrayShows().get(0).getDateStart());

		table.setBounds(228, 25, 696, 525);
		results = new JScrollPane(table);
		results.setBounds(228, 25, 696, 525);
		//contentPane.add(table, BorderLayout.EAST);
		contentPane.add(results,BorderLayout.EAST);
		JLabel lblTvadviser = new JLabel("TVAdviser");
		lblTvadviser.setFont(new Font("Tahoma", Font.PLAIN, 27));
		lblTvadviser.setBounds(78, 11, 125, 51);
		contentPane.add(lblTvadviser);

		JLabel lblNewLabel = new JLabel("Channel");
		lblNewLabel.setBounds(10, 71, 65, 25);
		contentPane.add(lblNewLabel);

		JLabel lblTitle = new JLabel("Title");
		lblTitle.setBounds(10, 100, 65, 14);
		contentPane.add(lblTitle);

		JLabel lblGenre = new JLabel("Genre");
		lblGenre.setBounds(10, 273, 65, 14);
		contentPane.add(lblGenre);

		JLabel lblSeason = new JLabel("Season");
		lblSeason.setBounds(10, 299, 65, 14);
		contentPane.add(lblSeason);

		JLabel lblEpisode = new JLabel("Episode");
		lblEpisode.setBounds(10, 325, 65, 14);
		contentPane.add(lblEpisode);

		JLabel lblDuration = new JLabel("Duration");
		lblDuration.setBounds(10, 126, 65, 14);
		contentPane.add(lblDuration);

		JLabel lblStartDate = new JLabel("Start Date");
		lblStartDate.setBounds(8, 166, 65, 14);
		contentPane.add(lblStartDate);

		JLabel lblDateEnd = new JLabel("Date End");
		lblDateEnd.setBounds(8, 191, 65, 14);
		contentPane.add(lblDateEnd);

		JLabel lblKeywords = new JLabel("Keywords");
		lblKeywords.setBounds(10, 244, 65, 25);
		contentPane.add(lblKeywords);

		String[] genres = { " ", "Generalista", "S�ries", "Infantil", "Desporto","Document�rios","Filmes","Informa��o","Entertenimento" };
		genre_combo = new JComboBox(genres);
		genre_combo.setSelectedIndex(4);
		genre_combo.setBounds(71, 273, 134, 20);
		contentPane.add(genre_combo);

		genre_combo.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JComboBox cb = (JComboBox)arg0.getSource();
				String genre = (String)cb.getSelectedItem();

				if(genre.equals("S�ries")){
					episodeField.setEditable(true);
					seasonField.setEditable(true);
				}else{
					episodeField.setEditable(false);
					seasonField.setEditable(false);
				}
			}

		});

		JButton search = new JButton("Search");
		search.setBounds(10, 481, 198, 25);
		contentPane.add(search);

		search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean allOk = true;
				database.resetClassification();
				database.resetParameters();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

				//temos de alterar isto para verificar se tem texto
				if(!channel_text.getText().equals("")){
					database.setInput_channel(channel_text.getText().toString());
					database.input_from(0);
				}

				if(!title_text.getText().equals("")){
					database.setInput_title(title_text.getText().toString());
					database.input_from(1);
				}

				if(!keyword_text.getText().equals("")){
					database.setInput_keyword(keyword_text.getText().toString());
					database.input_from(5);
				}


				database.setInput_genre(genre_combo.getSelectedItem().toString());
				System.out.println("GENRE -> " + database.getInput_genre());
				database.input_from(6);

				if(genre_combo.getSelectedItem().equals("S�ries") && 
						!seasonField.getText().equals("") && !episodeField.getText().equals("") ){
					database.setInput_season(seasonField.getText().toString());
					database.setInput_episode(episodeField.getText().toString());
					database.setInput_title(title_text.getText().toString() + " T" + seasonField.getText().toString()+" - Ep. "+ episodeField.getText().toString());
					System.err.println("Titulo: "+ database.getInput_title());
					database.input_from(7);
					database.input_from(8);
				}


				try {
					if(!start_date_text.getText().equals("")){
						database.setInput_date_start(sdf.parse(start_date_text.getText()));
						database.input_from(3);
					}
				} catch (ParseException e) {
					allOk = false;
					JOptionPane.showMessageDialog(null, "Start Date Not on Correct Format!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
					e.printStackTrace();
				}

				try {
					if(!end_date_text.getText().equals("")){
						database.setInput_date_end(sdf.parse(end_date_text.getText()));
						database.input_from(4);
					}
				} catch (ParseException e) {
					allOk = false;
					JOptionPane.showMessageDialog(null, "End Date Not on Correct Format!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
					e.printStackTrace();
				}

				try {
					if(!duration_text.getText().equals("")){
						try {
							database.setInput_duration_min(Integer.parseInt(duration_text.getText()));
							database.input_from(2);
						}
						catch (NumberFormatException e) {
							allOk = false;
							JOptionPane.showMessageDialog(null, "The Duration is not a number!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
						}

					}
				} catch (Exception e) {
					System.out.println("Deu bosta!");
					e.printStackTrace();
				}

				if(allOk){
					runDrools();
				}
			}
		});

		channel_text = new JTextField();
		channel_text.setBounds(71, 73, 134, 20);
		getContentPane().add(channel_text);
		channel_text.setColumns(10);

		title_text = new JTextField();
		title_text.setColumns(10);
		title_text.setBounds(71, 97, 134, 20);
		contentPane.add(title_text);

		/*genre_text = new JTextField();
		genre_text.setColumns(10);
		genre_text.setBounds(69, 121, 134, 20);
		contentPane.add(genre_text);
		 */
		duration_text = new JTextField();
		duration_text.setColumns(10);
		duration_text.setBounds(71, 125, 134, 20);
		contentPane.add(duration_text);

		start_date_text = new JTextField();
		start_date_text.setColumns(10);
		start_date_text.setBounds(71, 163, 134, 20);
		contentPane.add(start_date_text);

		end_date_text = new JTextField();
		end_date_text.setColumns(10);
		end_date_text.setBounds(71, 188, 134, 20);
		contentPane.add(end_date_text);

		keyword_text = new JTextField();
		keyword_text.setColumns(10);
		keyword_text.setBounds(71, 244, 134, 20);
		contentPane.add(keyword_text);

		seasonField = new JTextField();
		seasonField.setColumns(10);
		seasonField.setBounds(71, 299, 134, 20);
		contentPane.add(seasonField);

		episodeField = new JTextField();
		episodeField.setColumns(10);
		episodeField.setBounds(71, 325, 134, 20);
		contentPane.add(episodeField);

		episodeField.setEditable(false);
		seasonField.setEditable(false);

		JLabel lblConhecimentoERaciocnio = new JLabel("CR @ 2014/2015");
		lblConhecimentoERaciocnio.setHorizontalAlignment(SwingConstants.CENTER);
		lblConhecimentoERaciocnio.setBounds(10, 517, 208, 33);
		contentPane.add(lblConhecimentoERaciocnio);

		fillTable(1);
		setVisible(true);

	}

	private void fillTable(int option) {

		((DefaultTableModel) table.getModel()).setRowCount(0); //limpa tabela de resultados

		ArrayList<Show> shows = database.getArrayShows();
		ArrayList<Classification> classifications = database.getArrayClassification();			
		Show s;

		javax.swing.table.DefaultTableModel dtm = (javax.swing.table.DefaultTableModel)table.getModel();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		TimeZone gmtZone = TimeZone.getTimeZone("GMT");
		formatter.setTimeZone(gmtZone);

		switch(option){

		// All results
		case 1:
			for (int i=0; i<shows.size(); i++){
				dtm.addRow(new Object[]{classifications.get(i).getClassification(), shows.get(i).getName(), shows.get(i).getTitle(), shows.get(i).getGenre(), formatter.format(shows.get(i).getDateStart()), formatter.format(shows.get(i).getDateEnd()), shows.get(i).getDuration(), shows.get(i).getDescription()});
			}

			break;

			// Filtered results
		case 2:
			if(classifications.size()!=0)
			{
				for (int i=0; i<classifications.size(); i++){
					for (int j=0; j<shows.size(); j++){
						if (shows.get(j).getId() == classifications.get(i).getShowID()){
							if(classifications.get(i).getClassification() > 0){
								//System.out.println("Programa -> "+ classifications.get(j).getShowID() + " Classificacao -> " + classifications.get(j).getClassification());
								s = shows.get(j);
								//System.out.println("CANAL: "+s.getName()+" SHOW NAME: "+ s.getTitle() + " CLASSI: " +classifications.get(i).getClassification());
								dtm.addRow(new Object[]{classifications.get(i).getClassification(),s.getName(),s.getTitle(), s.getGenre() , formatter.format(s.getDateStart()), formatter.format(s.getDateEnd()),s.getDuration(),s.getDescription()});
							}
						}
					}
				}
				//System.out.println("SIZE -> " + classifications.size());

			}
			
			database.resetClassification();
			database.resetParameters();
			break;
		}

	}

	private void runDrools() {

		try {
			// load up the knowledge base
			KnowledgeBase kbase = readKnowledgeBase();
			StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
			KnowledgeRuntimeLogger logger = KnowledgeRuntimeLoggerFactory.newFileLogger(ksession, "test");
			// go !

			ksession.insert(database);

			for (int i=0; i<database.getArrayShows().size(); i++)
				ksession.insert(database.getArrayShows().get(i));

			System.out.println("inseriu dados..");

			ksession.fireAllRules();
			System.out.println("disparou!");
			logger.close();
		} catch (Throwable t) {
			t.printStackTrace();
		}

		database.sortClassifications();

		fillTable(2);

	}

	private static KnowledgeBase readKnowledgeBase() throws Exception {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		kbuilder.add(ResourceFactory.newClassPathResource("Rules.drl"), ResourceType.DRL);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		if (errors.size() > 0) {
			for (KnowledgeBuilderError error: errors) {
				System.err.println(error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
		return kbase;
	}
}
